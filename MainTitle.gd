extends Node2D

var time = 0
onready var LEVEL = preload("res://Level.tscn")

func _unhandled_input(event):
	if event is InputEventKey and time > 0.5:
		var level = LEVEL.instance()
		get_tree().get_root().add_child(level)
		queue_free()
		
		# load_game()
#		if level == 'res://Level.tscn':
#			get_tree().get_root().add_child(Level.instance())
#		elif level == 'res://Level2.tscn':
#			get_tree().get_root().add_child(Level2.instance())
#		elif level == 'res://Level3.tscn':
#			get_tree().get_root().add_child(Level3.instance())


func _process(delta):
	time += delta;
	$Sprite.material.set_shader_param("t", time)
