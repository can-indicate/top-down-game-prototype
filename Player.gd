extends KinematicBody2D


var speed = 5000.0
var is_running = false


func _ready():
	pass # Replace with function body.


func _physics_process(delta):
	if Input.is_action_just_released("ui_shift"):
		is_running = not is_running
	var velocity = Vector2(0.0, 0.0)
	if Input.is_action_pressed("key_d"):
		velocity += Vector2(1.0, 0.0)
	if Input.is_action_pressed("key_a"):
		velocity += Vector2(-1.0, 0.0)
	if Input.is_action_pressed("key_w"):
		velocity += Vector2(0.0, -1.0)
	if Input.is_action_pressed("key_s"):
		velocity += Vector2(0.0, 1.0)
	velocity = velocity.normalized()
	if is_running:
		velocity *= 2.0
	move_and_slide(speed*delta*velocity)
