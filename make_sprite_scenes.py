from glob import glob
import sys
import re

template_list = []
with open("animated_sprite_template.txt", 'r') as f:
    for line in f:
        if 'ext_resource' not in line:
            template_list.append(line)
        else:
            template_list.append('#')
template = ''.join(template_list)
# print(template)


folders = glob("./assets/**/**/", recursive=True)
for f in folders:
    frames = glob(f'{f}*.png')
    if len(frames) > 0:
        if len(frames) == 10:
            (frames[0], frames[1], frames[2], frames[3], 
             frames[4], frames[5], frames[6], frames[7],
             frames[8], frames[9]) = (
                 frames[0], frames[1], frames[2],
                 frames[6], frames[3], frames[7],
                 frames[5], frames[4], frames[8], frames[9])
        ext_resources = []
        for i, im in enumerate(frames):
            if 'win' in sys.platform:
                im2 = re.sub(r'\\', r'/', im)
            else:
                im2 = im
            res_path = f'res:/{im2[1::]}'
            ext_resource = f'[ext_resource path="{res_path}"'
            ext_resource += f' type="Texture" id={i+1}]\n'
            ext_resources.append(ext_resource)
        if 'win' in sys.platform:
            scene_name = re.sub(r'\\', '', f)
        scene_name = re.sub(r'\./assets', '', scene_name)
        scene_filename = scene_name + '.tscn'
        ext_resources_str = ''.join(ext_resources)
        scene_contents = re.sub(r'#+', ext_resources_str, template)
        scene_contents = re.sub('BodyWalkingOnehanded90Degrees',
                                scene_name, scene_contents)
        print(scene_contents)
        with open(scene_filename, 'w') as new_scene:
            new_scene.write(scene_contents)

