extends Control


var track_mouse = false
var prev_mouse_pos = Vector2.ZERO
var character_model
var player


const GENDER_SELECTIONS = ["Woman", "Man"]


var gender
var hair_styles
var clothing_legs
var clothing_torso
var footwear
var armwear
var beltwear
var neckwear
var weapon


const MAN_HAIR_STYLES = [
	"ManeAuburn",
	"ManeBlack",
	"ManeBlonde",
	"ManeBrown",
	"ManeBrunette",
	"ManeBurgundy",
	"ManeDark",
	"ManeDishwater",
	"ManeGinger",
	"ManeGray",
	"ManePurple",
	"ManeYellow",
	"MidpartAuburn",
	"MidpartBlack",
	"MidpartBlonde",
	"MidpartBrunette",
	"MidpartBurgundy",
	"MidpartCopper",
	"MidpartCrimson",
	"MidpartDeepblue",
	"MidpartGinger",
	"MidpartPlatinum",
	"MidpartPurple",
	"MidpartSilver", "None"
]
const MAN_CLOTHING_LEGS = [
	"PantsLeather",
	"BriefsBlack",
	"BriefsBlue",
	"BriefsGray",
	"BriefsGreen",
	"BriefsPink",
	"BriefsRed",
	"None"
]
const MAN_CLOTHING_TORSO = [
	"RobePeasant", "TopBarbarian", "TanktopWhite", "TanktopGray", "None"
]
const MAN_FOOTWEAR = [
	"BootsLeather", "BootsShinguard", "None"
]
const MAN_ARMWEAR = [
	"BracersBarbarian", "None"
]
const MAN_BELTWEAR = [
	"BeltSimple", "None"
]
const MAN_NECKWEAR = [
	"None"
]
const MAN_WEAPON = [
	"Mace", "Rapier", "None"
]


const WOMAN_HAIR_STYLES = [
	"MidbackBlack", "MidbackBlonde", "MidbackBlue",
	"MidbackBrown", "MidbackBrunette", "MidbackCrimson",
	"MidbackDark", "MidbackGinger", "MidbackPlatinum",
	"MidbackPurple", "MidbackSilver", "PixieAsh",
	"PixieBlack", "PixieBlonde", "PixieBlue",
	"PixieBrown", "PixieBrunette", "PixieCrimson",
	"PixieDishwater", "PixieGinger", "PixieGray",
	"PixieHazel", "PixiePink", "PixiePlatinum",
	"PixieRed", "PixieWhite", "PixieYellow", "None"
]
const WOMAN_CLOTHING_LEGS =[
	 "PantsSpanish", "PantsVelvet", "PantiesBlack",
	  "None"
]
const WOMAN_CLOTHING_TORSO = [
	"CorsetSpanish", "BraSportsBlack", "TopVelvet", "None"
]
const WOMAN_FOOTWEAR = ["ShoesWhite", "None"]
const WOMAN_ARMWEAR = ["BracersVelvet", "None"]
const WOMAN_BELTWEAR = ["SashSimple", "None"]
const WOMAN_NECKWEAR = ["ShawlCollared", "None"]
const WOMAN_WEAPON = ["Mace", "Rapier", "None"]

func update_options():
	character_model = get_parent().get_child(0).get_child(1)
	var option_container = $Panel/HBoxContainer/HBoxContainer
	if character_model.gender == "Woman":
		hair_styles = WOMAN_HAIR_STYLES
		clothing_legs = WOMAN_CLOTHING_LEGS
		clothing_torso = WOMAN_CLOTHING_TORSO
		footwear = WOMAN_FOOTWEAR
		armwear = WOMAN_ARMWEAR
		beltwear = WOMAN_BELTWEAR
		neckwear = WOMAN_NECKWEAR
		weapon = WOMAN_WEAPON
		var colours = ["Blue", "Gray", "Green", "Pink", "Red"]
		var more_clothing_torso = []
		var more_clothing_legs = []
		for c in colours:
			more_clothing_legs.append("Panties" + c)
			more_clothing_torso.append("BraSports" + c)
		clothing_legs = clothing_legs + more_clothing_legs
		clothing_torso = clothing_torso + more_clothing_torso
	elif character_model.gender == "Man":
		hair_styles = MAN_HAIR_STYLES
		clothing_legs = MAN_CLOTHING_LEGS
		clothing_torso = MAN_CLOTHING_TORSO
		footwear = MAN_FOOTWEAR
		armwear = MAN_ARMWEAR
		beltwear = MAN_BELTWEAR
		neckwear = MAN_NECKWEAR
		weapon = MAN_WEAPON
	gender = ([GENDER_SELECTIONS[0], GENDER_SELECTIONS[1]]
			 if character_model.gender == 'Woman' else
			 [GENDER_SELECTIONS[1], GENDER_SELECTIONS[0]])
	var options = [
		gender,
		hair_styles, clothing_torso, clothing_legs, footwear, armwear, 
		beltwear, neckwear, weapon
	]
	var options_gui = [
		$Panel/HBoxContainer/HBoxContainer/GenderOptions,
		$Panel/HBoxContainer/HBoxContainer/HairStyleOptions,
		$Panel/HBoxContainer/HBoxContainer/ClothingTorsoOptions,
		$Panel/HBoxContainer/HBoxContainer/ClothingLegsOptions,
		$Panel/HBoxContainer/HBoxContainer/FootwearOptions,
		$Panel/HBoxContainer/HBoxContainer/ArmwearOptions,
		$Panel/HBoxContainer/HBoxContainer/BeltwearOptions,
		$Panel/HBoxContainer/HBoxContainer/NeckwearOptions,
		$Panel/HBoxContainer/HBoxContainer/WeaponOptions
	]
	for i in range(len(options)):
		options_gui[i].clear()
		for k in range(len(options[i])):
			options_gui[i].add_item(options[i][k], k)
	character_model.hair = "Hair" + hair_styles[0]
	character_model.clothing_legs = clothing_legs[0]
	character_model.clothing_torso = clothing_torso[0]
	character_model.footwear = footwear[0]
	character_model.armwear = armwear[0]
	character_model.beltwear = beltwear[0]
	character_model.neckwear = neckwear[0] if neckwear[0] != "None" else ""
	character_model.weapon = weapon[0]

func _ready():
	update_options()

func _process(delta):
	if track_mouse:
		var curr_mouse_pos = get_global_mouse_position()
		var mouse_delta = curr_mouse_pos - prev_mouse_pos
		set_position(get_position() + mouse_delta)
		prev_mouse_pos = curr_mouse_pos

func _on_TextureButton_button_up():
	track_mouse = not track_mouse


func _on_TextureButton_button_down():
	prev_mouse_pos = get_global_mouse_position()
	get_parent().move_child(self, get_parent().get_child_count()-1)
	track_mouse = not track_mouse


func _on_Button5_button_up():
	queue_free()


func _on_OptionButton_item_selected(index):
	character_model.gender = gender[index]
	update_options()


func _on_HairStyleOptions_item_selected(index):
	var hair = "Hair" + hair_styles[index]
	character_model.hair = "Hair" + hair_styles[index]
	if hair == "HairNone":
		character_model.hair = ""


func _on_ClothingLegsOptions_item_selected(index):
	var clothing = clothing_legs[index]
	character_model.clothing_legs = clothing
	if clothing == "None":
		character_model.clothing_legs = ""


func _on_ClothingTorsoOptions_item_selected(index):
	var clothing = clothing_torso[index]
	character_model.clothing_torso = clothing
	if clothing == "None":
		character_model.clothing_torso = ""


func _on_FootwearOptions_item_selected(index):
	var e = footwear[index]
	character_model.footwear = e
	if e == "None":
		character_model.footwear = ""


func _on_ArmwearOptions_item_selected(index):
	var e = armwear[index]
	character_model.armwear = e
	if e == "None":
		character_model.armwear = ""


func _on_BeltwearOptions_item_selected(index):
	var e = beltwear[index]
	character_model.beltwear = e
	if e == "None":
		character_model.beltwear = ""


func _on_NeckwearOptions_item_selected(index):
	var e = neckwear[index]
	character_model.neckwear = e
	if e == "None":
		character_model.neckwear = ""


func _on_WeaponOptions_item_selected(index):
	var e = weapon[index]
	character_model.weapon = e
	if e == "None":
		character_model.weapon = ""


func _on_HSlider_value_changed(value):
	var player = get_parent().get_child(0)
	player.scale = Vector2(value, value)

