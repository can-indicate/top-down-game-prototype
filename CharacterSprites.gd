extends AnimatedSprite



const BASE = "res://assets/large/"
var gender = "Man"
var body = "Body"
var hair = "ManeAuburn"
var clothing_legs = "PantsSpanish"
var clothing_torso = "CorsetSpanish"
var footwear = "ShoesWhite"
var armwear = "BracersVelvet"
var beltwear = "SashSimple"
var neckwear = "ShawlCollared"
var weapon = "Mace"
const DEG_0 = "0Degrees"
const DEG_45 = "45Degrees"
const DEG_90 = "90Degrees"
const DEG_135 = "135Degrees"
const DEG_180 = "180Degrees"
const DEG_225 = "225Degrees"
const DEG_270 = "270Degrees"
const DEG_315 = "315Degrees"


var delta_frame = 0.0
var max_delta_frame = 1.0/15.0
var frame_id = 0
var deg = DEG_0
var velocity = Vector2(0.0, 0.0)
var is_running = false

func get_texture_from_image(fname: String) -> ImageTexture:
	var file = File.new()
	file.open(fname, File.READ)
	var length = file.get_len()
	var content = file.get_buffer(length)
	var image = Image.new()
	var texture = ImageTexture.new()
	image.load_png_from_buffer(content)
	texture.create_from_image(image)
	return texture


func _ready():
	for e in get_children():
		e.scale = Vector2(1.0, 1.0)
	
func _process(delta):
	var walking = "Walking" if gender == "Woman" else "Walk"
	var running = "Running" if gender == "Woman" else "Walk"
	var idle = "Idle" if gender == "Woman" else "Idle"
	var attack = "AttackOnehanded" if gender == "Woman" else "Attack"
	var pick_up = "PickingUp" if gender == "Woman" else ""
	var die = "Dying" if gender == "Woman" else "Die"
	if gender == "Woman":
		if weapon != "":
			walking += "Onehanded" 
			running += "Onehanded" 
			idle +=  "Onehanded" 
			die += "Onehanded"
		else:
			walking += "Unarmed" 
			running += "Unarmed" 
			idle +=  "Unarmed" 
			die += "Unarmed"
	var move = idle
	var velocity = Vector2(0.0, 0.0)
	if Input.is_action_pressed("ui_accept"):
		move = attack
	if Input.is_action_pressed("key_e"):
		move = pick_up
	if Input.is_action_pressed("key_x"):
		move = die
		if frame_id == 9:
			frame_id -= 1
	if Input.is_action_just_released("ui_shift"):
		is_running = not is_running
	if Input.is_action_pressed("key_d"):
		velocity += Vector2(1.0, 0.0)
	if Input.is_action_pressed("key_a"):
		velocity += Vector2(-1.0, 0.0)
	if Input.is_action_pressed("key_w"):
		velocity += Vector2(0.0, 1.0)
	if Input.is_action_pressed("key_s"):
		velocity += Vector2(0.0, -1.0)
	if velocity.length() != 0.0:
		move = walking if not is_running else running
		var angle = 8.0*velocity.angle()/PI
		print(angle)
		if angle > -5.0 and angle <= -3.0:
			deg = DEG_0
		elif angle > -3.0 and angle <= -1.0:
			deg = DEG_45
		elif angle <= 1.0 and angle > -1.0:
			deg = DEG_90
		elif angle > 1.0 and angle <= 3.0:
			deg = DEG_135
		elif angle > 3.0 and angle <= 5.0:
			deg = DEG_180
		elif angle > 5.0 and angle <= 7.0:
			deg = DEG_225
		elif (angle > 7.0 and angle <= 9.0) or (angle > -8.0 and angle <= -7.0):
			deg = DEG_270
		elif (angle > -7.0 and angle <= -5.0):
			deg = DEG_315
	if delta_frame >= max_delta_frame:
		delta_frame = 0.0
		# <gender>/<body>/<movement>/<angle>/Frame<index>.png
		var format = BASE + "/%s/%s/%s/%s/Frame%d.png"
		var string = ""
		
		string = format % [gender, body, move, deg, frame_id]
		var texture = get_texture_from_image(string)
		$Body.texture = texture
		if hair != "":
			var move2 = move
			if (move == pick_up and ("Mid" in hair)):
				move2 = "PickupUnarmed"
			string = format % [gender, hair, move2, deg, frame_id]
			var texture2 = get_texture_from_image(string)
			$Hair.texture = texture2
		else:
			$Hair.texture = null
			
		if clothing_legs != "":
			var move2 = move
			if (move == pick_up):
				move2 = "PickupUnarmed"
			string = format % [gender, clothing_legs, move2, deg, frame_id]
			var texture3 = get_texture_from_image(string)
			$ClothingLegs.texture = texture3
		else:
			$ClothingLegs.texture = null
			
		if clothing_torso != "":
			var move2 = move
			if (move == pick_up):
				move2 = "PickupUnarmed"
			string = format % [gender, clothing_torso, move2, deg, frame_id]
			var texture4 = get_texture_from_image(string)
			$ClothingTorso.texture = texture4
		else:
			$ClothingTorso.texture = null
			
		if footwear != "":
			var move2 = move
			if (move == pick_up):
				move2 = "PickupUnarmed"
			string = format % [gender, footwear, move2, deg, frame_id]
			var texture5 = get_texture_from_image(string)
			$Footwear.texture = texture5
		else:
			$Footwear.texture = null
			
		if weapon != "" and move != pick_up:
			var move2 = move
			if (move == pick_up):
				move2 = "PickupUnarmed"
			string = format % [gender, weapon, move2, deg, frame_id]
			var texture6 = get_texture_from_image(string)
			$Weapons.texture = texture6
		else:
			$Weapons.texture = null
		
		if armwear != "":
			var move2 = move
			if (move == pick_up):
				move2 = "PickupUnarmed"
			string = format % [gender, armwear, move2, deg, frame_id]
			var texture7 = get_texture_from_image(string)
			$Armwear.texture = texture7
		else:
			$Armwear.texture = null
		
		if beltwear != "":
			var move2 = move
			if (move == pick_up):
				move2 = "PickupUnarmed"
			string = format % [gender, beltwear, move2, deg, frame_id]
			var texture8 = get_texture_from_image(string)
			$Beltwear.texture = texture8
		else:
			$Beltwear.texture = null
			
		if neckwear != "":
			print(neckwear)
			var move2 = move
			if (move == pick_up):
				move2 = "PickupUnarmed"
			string = format % [gender, neckwear, move2, deg, frame_id]
			var texture9 = get_texture_from_image(string)
			$Neckwear.texture = texture9
		else:
			$Neckwear.texture = null
		
		frame_id += 1
	frame_id = frame_id % 10
	delta_frame += delta
	



